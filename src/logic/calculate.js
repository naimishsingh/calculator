import Big from 'big.js'

function isValidNumber(item){
    return /[0-9]+/.test(item);
}

function operate(numOne, numTwo, operation){

    const one = Big(numOne || 0);
    const two = Big(numTwo || (operation === '/' || operation === 'x' ? 1 : 0));

    if(operation === '+'){
        return one.plus(two).toString();
    }else if(operation === '-'){    
        return one.minus(two).toString();
    }else if(operation === 'x'){
        return one.times(two).toString();
    }else if(operation === '/'){
        if(two === '0'){
            alert('Divide by 0 error!!');
            return '0';
        }else{
            return one.div(two).toString();
        }
    }else{
        throw Error(`Unknown operation '${operation}'`);
    }
}

function calculate(obj, buttonName){
    if(buttonName === 'AC'){
        return {
            total : null,
            next : null,
            operation : null
        }
    }

    if(isValidNumber(buttonName)){
        if(buttonName === '0' && obj.next === '0'){
            return {};
        }
        if(obj.operation){
            if(obj.next){
                return {next : obj.next + buttonName};
            }
            return {next : buttonName};
        }
        if(obj.next){
            const next = obj.next === '0' ? buttonName : obj.next + buttonName;
            return{
                next,
                total: null
            }
        }
        return{
            next : buttonName,
            total : null
        }
    }
    if(buttonName === '%'){
        if(obj.operation && obj.next){
            const result = operate(obj.total, obj.next, obj.operation);
            return{
                total : Big(result).div(Big('100')).toString(),
                next : null,
                operation : null
            }
        }
        if(obj.next){
            return{
                next : Big(obj.next).div(Big('100')).toString()
            }
        }
        return {};
    }
    if(buttonName === '.'){
        if(obj.next){
            if(obj.next.includes('.')){
                return {}
            }
            return {next : obj.next+'.'}
        }
        return {next:'0.'}
    }
    if(buttonName === '='){
        if(obj.next && obj.operation){
            return{
                total : operate(obj.total, obj.next, obj.operation),
                next : null,
                operation : null
            }
        }else{
            return {}
        }
    }
    if(buttonName === '+/-'){
        if(obj.next){
            return {next:(-1 * parseFloat(obj.next)).toString()}
        }
        if(obj.total){
            return {next:(-1 * parseFloat(obj.total)).toString()}
        }
        return {}
    }
    if(obj.operation){
        return{
            total : operate(obj.total, obj.next, obj.operation),
            next : null,
            operation : buttonName
        }
    }
    if(!obj.next){
        return {operation : buttonName}
    }
    return{
        total : obj.next,
        next : null,
        operation : buttonName
    }
}

export default calculate;