import React, {Component} from 'react';
import './index.css';

class Display extends Component{

    render(){
        return(
            <div className='display'>
                <div>{this.props.value}</div>
            </div>
        )
    }
}

export default Display;